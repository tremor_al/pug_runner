// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "runnerGameMode.h"
#include "runnerCharacter.h"
#include "UObject/ConstructorHelpers.h"

ArunnerGameMode::ArunnerGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
