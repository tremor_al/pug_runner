// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "runnerGameMode.generated.h"

UCLASS(minimalapi)
class ArunnerGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ArunnerGameMode();
};



