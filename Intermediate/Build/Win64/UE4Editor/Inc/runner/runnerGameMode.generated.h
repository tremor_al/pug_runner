// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef RUNNER_runnerGameMode_generated_h
#error "runnerGameMode.generated.h already included, missing '#pragma once' in runnerGameMode.h"
#endif
#define RUNNER_runnerGameMode_generated_h

#define runner_Source_runner_runnerGameMode_h_12_RPC_WRAPPERS
#define runner_Source_runner_runnerGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define runner_Source_runner_runnerGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesArunnerGameMode(); \
	friend RUNNER_API class UClass* Z_Construct_UClass_ArunnerGameMode(); \
public: \
	DECLARE_CLASS(ArunnerGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/runner"), RUNNER_API) \
	DECLARE_SERIALIZER(ArunnerGameMode) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define runner_Source_runner_runnerGameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesArunnerGameMode(); \
	friend RUNNER_API class UClass* Z_Construct_UClass_ArunnerGameMode(); \
public: \
	DECLARE_CLASS(ArunnerGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/runner"), RUNNER_API) \
	DECLARE_SERIALIZER(ArunnerGameMode) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define runner_Source_runner_runnerGameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	RUNNER_API ArunnerGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ArunnerGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(RUNNER_API, ArunnerGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ArunnerGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	RUNNER_API ArunnerGameMode(ArunnerGameMode&&); \
	RUNNER_API ArunnerGameMode(const ArunnerGameMode&); \
public:


#define runner_Source_runner_runnerGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	RUNNER_API ArunnerGameMode(ArunnerGameMode&&); \
	RUNNER_API ArunnerGameMode(const ArunnerGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(RUNNER_API, ArunnerGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ArunnerGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ArunnerGameMode)


#define runner_Source_runner_runnerGameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define runner_Source_runner_runnerGameMode_h_9_PROLOG
#define runner_Source_runner_runnerGameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	runner_Source_runner_runnerGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	runner_Source_runner_runnerGameMode_h_12_RPC_WRAPPERS \
	runner_Source_runner_runnerGameMode_h_12_INCLASS \
	runner_Source_runner_runnerGameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define runner_Source_runner_runnerGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	runner_Source_runner_runnerGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	runner_Source_runner_runnerGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	runner_Source_runner_runnerGameMode_h_12_INCLASS_NO_PURE_DECLS \
	runner_Source_runner_runnerGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID runner_Source_runner_runnerGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
