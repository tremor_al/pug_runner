// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef RUNNER_runnerCharacter_generated_h
#error "runnerCharacter.generated.h already included, missing '#pragma once' in runnerCharacter.h"
#endif
#define RUNNER_runnerCharacter_generated_h

#define runner_Source_runner_runnerCharacter_h_12_RPC_WRAPPERS
#define runner_Source_runner_runnerCharacter_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define runner_Source_runner_runnerCharacter_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesArunnerCharacter(); \
	friend RUNNER_API class UClass* Z_Construct_UClass_ArunnerCharacter(); \
public: \
	DECLARE_CLASS(ArunnerCharacter, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/runner"), NO_API) \
	DECLARE_SERIALIZER(ArunnerCharacter) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define runner_Source_runner_runnerCharacter_h_12_INCLASS \
private: \
	static void StaticRegisterNativesArunnerCharacter(); \
	friend RUNNER_API class UClass* Z_Construct_UClass_ArunnerCharacter(); \
public: \
	DECLARE_CLASS(ArunnerCharacter, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/runner"), NO_API) \
	DECLARE_SERIALIZER(ArunnerCharacter) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define runner_Source_runner_runnerCharacter_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ArunnerCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ArunnerCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ArunnerCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ArunnerCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ArunnerCharacter(ArunnerCharacter&&); \
	NO_API ArunnerCharacter(const ArunnerCharacter&); \
public:


#define runner_Source_runner_runnerCharacter_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ArunnerCharacter(ArunnerCharacter&&); \
	NO_API ArunnerCharacter(const ArunnerCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ArunnerCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ArunnerCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ArunnerCharacter)


#define runner_Source_runner_runnerCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CameraBoom() { return STRUCT_OFFSET(ArunnerCharacter, CameraBoom); } \
	FORCEINLINE static uint32 __PPO__FollowCamera() { return STRUCT_OFFSET(ArunnerCharacter, FollowCamera); }


#define runner_Source_runner_runnerCharacter_h_9_PROLOG
#define runner_Source_runner_runnerCharacter_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	runner_Source_runner_runnerCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	runner_Source_runner_runnerCharacter_h_12_RPC_WRAPPERS \
	runner_Source_runner_runnerCharacter_h_12_INCLASS \
	runner_Source_runner_runnerCharacter_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define runner_Source_runner_runnerCharacter_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	runner_Source_runner_runnerCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	runner_Source_runner_runnerCharacter_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	runner_Source_runner_runnerCharacter_h_12_INCLASS_NO_PURE_DECLS \
	runner_Source_runner_runnerCharacter_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID runner_Source_runner_runnerCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
